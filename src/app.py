
#Every module in python has a attribute which is called as name . The value of name 
# attribute is 'main' when module run directly. 
#Otherwise the value of name is the name of the module.

def squares(x):
    y=x**2
    return y

if __name__=="__main__":
    print("the square of 10 is %s" %squares(10))