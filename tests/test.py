import unittest
from src.app import squares

class AppTests(unittest.TestCase):
    def test_app(self):
        """Simple tests"""
        self.assertEqual(squares(10),100)
        self.assertNotEqual(squares(4),15)
    def test_errors(self):
        """check method fails when parameter not number"""
        with self.assertRaises(TypeError):
            squares("foo")

    def suite():
        _suite=unittest.TestSuite()
        _suite.addTest(AppTests('test_app'))
        _suite.addTest(AppTests('test_errors'))
        return _suite
